<?php  	
	/*
		Preheader
	*/
	function pmpron_manage_sites_preheader() {
		if(!is_admin()) {
			global $post, $current_user;
			if(!empty($post->post_content) && strpos($post->post_content, "[pmpron_manage_sites]") !== false) {
				/*
					Preheader operations here.
				*/

				//make sure they have site credits
				global $current_user;
				$credits = $current_user->pmpron_site_credits;
				
				if(empty($credits)) {
					//redirect to levels
					wp_redirect(pmpro_url("levels"));
					exit;
				}
			}
		}
	}
	add_action("wp", "pmpron_manage_sites_preheader", 1);	
	
	/*
		Shortcode Wrapper
	*/
	function pmpron_manage_sites_shortcode($atts, $content=null, $code="") {			
		ob_start();		
		
		global $current_user, $pmpro_msg, $pmpro_msgt;			
		
		//adding a site?
		if(!empty($_POST['addsite'])) {
			$sitename = $_REQUEST['sitename'];
			$sitetitle = $_REQUEST['sitetitle'];
			if(pmpron_checkSiteName( $sitename, $sitetitle )) {
				$blog_id = pmpron_addSite($sitename, $sitetitle);
				if(is_wp_error($blog_id)) {
					$pmpro_msg = "Error creating site.";
					$pmpro_msgt = "pmpro_error"; 
				} else {
					$pmpro_msg = "Your site has been created.";
					$pmpro_msgt = "pmpro_success";
				}
			} else {
				//error message shown below
			}			
		} else {
			//default values for form
			$sitename = '';
			$sitetitle = '';
		}

		//show page		
		$blog_ids = pmpron_getBlogsForUser($current_user->ID);

		?>
		<style>
			.pmpro_sites_list {
				margin-bottom: 2em;
			}
			.pmpro_sites_list-site {
				align-items: center;
				border-bottom: 1px solid #eee;
				display: flex;
				flex-wrap: wrap;
				justify-content: space-between;
				padding: 1em 0;
			}
			.pmpro_sites_list-site .pmpro_sites_list-title {
				margin-bottom: 0;
			}
			.pmpro_add_site_input {
				margin-bottom: 2em;
			}
			.pmpro_add_site_input p {
				margin-bottom: .5em;
			}
			.pmpro_add_site_input_helper {
				display: block;
			}
		</style>
		
		<?php if(!empty($blog_ids)) { ?>
		<h2><?php _e('Your Sites', 'pmpro-network'); ?></h2>
		<?php if(!empty($pmpro_msg)) { ?>
			<div class="pmpro_message <?php echo $pmpro_msgt;?>"><?php echo $pmpro_msg;?></div>
		<?php } ?>
		<div class="pmpro_message <?php if( count($blog_ids) >= intval($current_user->pmpron_site_credits) ) { ?>pmpro_error<?php } ?>">
			<?php if( count($blog_ids) >= intval($current_user->pmpron_site_credits) ) { ?>
				<?php echo sprintf('You have used %s of %s site credits.', intval($current_user->pmpron_site_credits), intval($current_user->pmpron_site_credits), 'pmpro-network'); ?>
				<?php if(count($blog_ids) > intval($current_user->pmpron_site_credits)) { ?>
					<?php echo sprintf('%s of your sites have been deactivated.', count($blog_ids) - intval($current_user->pmpron_site_credits), 'pmpro-network'); ?>
				<?php } ?> 
			<?php } else { ?>
				<?php echo sprintf('You have used %s of %s site credits.', count($blog_ids), intval($current_user->pmpron_site_credits), 'pmpro-network'); ?>
			<?php } ?>
		</div>
		
		<div class="pmpro_sites_list">
			<?php foreach($blog_ids as $blog_id) { ?>
				<?php if( ! get_blog_details($blog_id)) continue; ?>
				<div class="pmpro_sites_list-site<?php if(get_blog_status($blog_id, "deleted")) { ?> pmpro_grey<?php } ?>">
					<div class="pmpro_sites_list-site-info">
						<?php if(get_blog_status($blog_id, "deleted")) { ?>
							<h5 class="pmpro_sites_list-title">
								<?php echo get_blog_option($blog_id, "blogname"); ?>
							</h5>
						<?php } else { ?>
							<h5 class="pmpro_sites_list-title">
								<a href="<?php echo get_site_url($blog_id);?>"><?php echo get_blog_option($blog_id, "blogname");?></a>
							</h5>
							<span class="pmpro_sites_list-url"><?php echo get_site_url($blog_id);?></span>
						<?php } ?>
					</div>
					<div class="pmpro_sites_list-site-links">
					<?php 
							if(get_blog_status($blog_id, "deleted")) 
							{ 
								?>
								<?php _e('(deactivated)', 'pmpro-network'); ?>
								<?php 
							} 
							else
							{
								?>
								<a href="<?php echo get_site_url($blog_id);?>"><?php _e('Visit', 'pmpro-network'); ?></a>&nbsp;|&nbsp;<a href="<?php echo get_site_url($blog_id);?>/wp-admin/"><?php _e('Dashboard', 'pmpro-network'); ?></a>
								<?php
							}
						?>
					</div>
				</div>
				<?php } ?>
		</div>
		<?php } ?>

		<?php if($current_user->pmpron_site_credits > count($blog_ids)) { ?>
		<form class="pmpro_form" id="pmpro_add_site_form" action="" method="post">
			<div class="pmpro_checkout">
				<h4><?php _e('Add a Site', 'pmpro-network'); ?></h4>
				<div class="pmpro_add_site_input">
					<label for="sitename"><?php _e('Site Name', 'pmpro-network'); ?></label>
					<p>Must be at least 4 characters (letters/numbers only). Once your site is created the site name cannot be changed.</p>
					<input id="sitename" name="sitename" type="text" class="input" size="30" value="<?php echo esc_attr(stripslashes($sitename)); ?>" />
					<small class="pmpro_add_site_input_helper"><em>
						<?php
							global $current_site;
							$site_domain = preg_replace( '|^www\.|', '', $current_site->domain );
						
							if ( !is_subdomain_install() )
								$site = $current_site->domain . $current_site->path . __( 'sitename' );
							else
								$site = __( '{site name}' ) . '.' . $site_domain . $current_site->path;
		
							echo sprintf( __('Your address will be %s'), $site );					
						?>
					</em></small>
				</div>
				<div class="pmpro_add_site_input">
					<label for="sitetitle"><?php _e('Site Title', 'pmpro-network'); ?></label>
					<input id="sitetitle" name="sitetitle" type="text" class="input" size="30" value="<?php echo esc_attr(stripslashes($sitetitle)); ?>" />
				</div>
				<input type="hidden" name="addsite" value="1" />
				<div class="pmpro_add_site_input">
					<input type="submit" name="submit" value="Add Site" />
				</div>
			</div>
		</form>

		<?php } ?>

		<?php
		$temp_content = ob_get_contents();
		ob_end_clean();
		return $temp_content;			
	}
	add_shortcode( 'pmpron_manage_sites', 'pmpron_manage_sites_shortcode');
	